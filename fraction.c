#include <string.h>
#include <stdio.h>
#include <stdlib.h>

struct fraction {
    float numerator;
    float denominator;
};
double fractionToDecimal(struct fraction f) {
    return f.numerator / f.denominator;
}
struct fraction decimalToFraction(double x) {
    int ex = extractor(x);
    int places = countDecimalPlaces(x);
    int gcd = gcd(ex, places);
    struct fraction fr = {ex / gcd, powN(10, places) / gcd};
    return fr;
}
/*
char extractor1(double x) {
    int result = (int)(x * 10) % 10;
    return (char)result;
}
char extractor2(double x) {
    int result = (int)(x * 100) % 10;
    return (char)result;
}
char extractor3(double x) {
    int result = (int)(x * 1000) % 10;
    return (char)result;
}
*/
int extractor(double x) {
    char extract[10] = "0"
    for (int i = 10; i < 1000000001 ; i *= 10) {
        int result = (int)(x * i) % 10;
        char ex = '0' + result;
        strcat(extract, ex);
    }
    int extractInt = atoi(extract);
    return extractInt;
}
int countDecimalPlaces(double number) {
    int decimal_places = 0;
    double fraction = number - (int)number;
    if (fraction != 0.0) {
        for (int i = 0; i == 10; i++) {
            fraction *= 10.0;
            decimal_places++;
            fraction -= (int)fraction;
        }
    }
    return decimal_places;
}
int gcd(int a, int b) {
    if (b == 0) {
        return a;
    }
    
    return gcd(b, a % b);
}